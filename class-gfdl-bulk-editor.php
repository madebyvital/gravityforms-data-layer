<?php
class GFDL_Bulk_Editor
{
    private static function get_sales_cycle_options()
    {
        return [
            ["label" => "None", "value" => "None"],
            ["label" => "Early Stage", "value" => "Early Stage"],
            ["label" => "Sales Ready", "value" => "Sales Ready"],
        ];
    }

    public static function render_bulk_editor()
    {
        if (!current_user_can("manage_options")) {
            wp_die(
                __(
                    "You do not have sufficient permissions to access this page."
                )
            );
        }

        if (
            $_SERVER["REQUEST_METHOD"] === "POST" &&
            isset($_POST["gfdl_bulk_update"]) &&
            check_admin_referer("gfdl_bulk_editor", "gfdl_nonce")
        ) {
            self::save_bulk_editor();
        }

        $forms = GFAPI::get_forms();
        $sales_cycle_options = self::get_sales_cycle_options();
        ?>
        <div class="wrap gfdl-bulk-editor">
            <div class="gfdl-header">
                <h1><span class="dashicons dashicons-forms"></span> Bulk Edit Data Layers</h1>
                <p class="description">Update sales cycle settings for all your forms in one place.</p>
            </div>

            <?php settings_errors("gfdl_bulk_editor"); ?>

            <div class="gfdl-card">
                <form method="POST" id="gfdl-bulk-editor">
                    <div class="table-container">
                        <table class="wp-list-table widefat fixed striped">
                            <thead>
                                <tr>
                                    <th class="column-form-id">Form ID</th>
                                    <th class="column-form-name">Form Name</th>
                                    <th class="column-sales-cycle">Sales Cycle</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($forms as $form):
                                    $selected_option = isset($form["gfdl"]["sales_cycle"])
                                        ? $form["gfdl"]["sales_cycle"]
                                        : "None";
                                    $status_class = $selected_option !== "None"
                                        ? "has-status"
                                        : "";
                                ?>
                                <tr class="<?php echo esc_attr($status_class); ?>">
                                    <td class="column-form-id">
                                        <strong>#<?php echo esc_html($form["id"]); ?></strong>
                                    </td>
                                    <td class="column-form-name">
                                        <div class="form-title">
                                            <?php echo esc_html($form["title"]); ?>
                                        </div>
                                        <div class="row-actions">
                                            <span class="edit">
                                                <a href="<?php echo esc_url(
                                                    admin_url(
                                                        "admin.php?page=gf_edit_forms&id=" .
                                                            $form["id"]
                                                    )
                                                ); ?>">
                                                    Edit Form
                                                </a>
                                            </span>
                                        </div>
                                    </td>
                                    <td class="column-sales-cycle">
                                        <select name="gfdl_sales_cycle[<?php echo esc_attr(
                                            $form["id"]
                                        ); ?>]"
                                                class="gfdl-select <?php echo strtolower(
                                                    str_replace(" ", "-", $selected_option)
                                                ); ?>">
                                            <?php foreach ($sales_cycle_options as $option): ?>
                                                <option value="<?php echo esc_attr(
                                                    $option["value"]
                                                ); ?>"
                                                    <?php selected(
                                                        $selected_option,
                                                        $option["value"]
                                                    ); ?>>
                                                    <?php echo esc_html($option["label"]); ?>
                                                </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>

                    <div class="gfdl-footer">
                        <?php wp_nonce_field("gfdl_bulk_editor", "gfdl_nonce"); ?>
                        <div class="gfdl-actions">
                            <button type="submit" name="gfdl_bulk_update" class="button button-primary button-large">
                                <span class="dashicons dashicons-yes-alt"></span> Save Changes
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <?php
    }

    public static function save_bulk_editor()
    {
        if (!isset($_POST["gfdl_sales_cycle"])) {
            return;
        }

        $success_count = 0;
        $error_count = 0;

        foreach ($_POST["gfdl_sales_cycle"] as $form_id => $sales_cycle) {
            try {
                // Get the form
                $form = GFAPI::get_form($form_id);
                if (!$form) {
                    $error_count++;
                    continue;
                }

                // Initialize or update the gfdl settings array
                if (!isset($form['gfdl']) || !is_array($form['gfdl'])) {
                    $form['gfdl'] = array();
                }

                // Preserve any existing data layer properties
                $existing_properties = isset($form['gfdl']['data_layer_properties']) 
                    ? $form['gfdl']['data_layer_properties'] 
                    : array();

                // Update the settings
                $form['gfdl'] = array(
                    'sales_cycle' => sanitize_text_field($sales_cycle),
                    'data_layer_properties' => $existing_properties
                );

                // Save the updated form
                $result = GFAPI::update_form($form);
                
                if (is_wp_error($result)) {
                    $error_count++;
                } else {
                    $success_count++;
                }
            } catch (Exception $e) {
                $error_count++;
            }
        }

        // Set appropriate message based on results
        if ($error_count === 0) {
            add_settings_error(
                'gfdl_bulk_editor',
                'settings_updated',
                sprintf(__('%d forms updated successfully.'), $success_count),
                'updated'
            );
        } elseif ($success_count === 0) {
            add_settings_error(
                'gfdl_bulk_editor',
                'settings_error',
                __('Error: No forms were updated.'),
                'error'
            );
        } else {
            add_settings_error(
                'gfdl_bulk_editor',
                'settings_partial',
                sprintf(
                    __('%d forms updated successfully, %d forms failed to update.'), 
                    $success_count, 
                    $error_count
                ),
                'warning'
            );
        }
    }
}
?>