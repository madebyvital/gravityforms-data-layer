# GTM Data Layer Add-On for Gravity Forms

## Overview

The GTM Data Layer Add-On is a powerful and flexible extension for Gravity Forms, designed to allow tracking GA event tracking through Google Tag Manager. 

### Features

- **Automatic Data Layer Integration:** Simplifies the process of sending form data to the Google Tag Manager Data Layer.
- **Customizable Data Layer Content:** Allows for the customization of the data sent to the Data Layer, including additional key-value pairs for detailed tracking.
- **Enhanced Form Tracking:** Tracks various form events, including text and URL confirmations.
- **Secure Data Handling:** Utilizes encryption for sensitive data to ensure privacy and security.
- **Google Analytics Integration:** Supports the Google Analytics Measurement Protocol for advanced tracking capabilities.
- **Easy to Use:** Designed with a user-friendly interface, making it accessible even for non-technical users.

## Installation

1. Ensure Gravity Forms is installed and activated on your WordPress site.
2. Download the GTM Data Layer Add-On plugin.
3. Navigate to your WordPress admin dashboard, go to Plugins -> Add New -> Upload Plugin.
4. Choose the downloaded plugin file and click 'Install Now.'
5. After installation, activate the plugin.

## Configuration

After activating the plugin, configure it by navigating to the Gravity Forms settings page. Here, you can set up various options like Lead Stage, Data Layer Properties, and more.

### Setting Up Lead Stage

1. Navigate to the 'Form Settings' of the desired form.
2. Under 'Lead Stage', select the appropriate stage for your form.

### Adding Key Value Pairs

1. In the 'Form Settings', find the 'Additional Key Value Pairs' section.
2. Enter your desired key-value pairs that will be pushed to the Data Layer upon form submission.

## Usage

Once configured, the plugin automatically pushes data to the Google Tag Manager Data Layer whenever a form is submitted. You can view this data within your GTM dashboard and utilize it for detailed analytics and tracking.

### Data Layer Customization

The data pushed to the Data Layer can be customized per form, allowing you to track specific information relevant to each form.

### Security

The plugin ensures the security of your data by using encryption for sensitive information.

## Troubleshooting

If you encounter any issues:

- Ensure that all plugin requirements are met and that it's properly configured.
- Check for any plugin conflicts, especially with other Gravity Forms add-ons.
- Review the JavaScript console in your web browser for any errors.

---

### Version

- Plugin Version: `2`
- Minimum Gravity Forms Version Required: `2.5`

### Author

- This plugin is developed and maintained by the Vital Dev Team.



