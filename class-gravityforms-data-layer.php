<?php
if (!defined('ABSPATH')) {
	exit;
}

GFForms::include_addon_framework();

class GFDL extends GFAddOn {

	protected $_version = GFDL_VERSION;
	protected $_min_gravityforms_version = '2.5';
	protected $_slug = 'gfdl';
	protected $_path = 'gravityforms-data-layer/gravityforms-data-layer.php';
	protected $_full_path = __FILE__;
	protected $_title = 'GTM Data Layer';
	protected $_short_title = 'Data Layer';
	protected $_salt= 'B4857272054C82F3D60A2530E4039DA9E8F64A59D78B0D41C04E69DABC19065E';

	private static $_instance = null;

	static function get_instance() {
		if (self::$_instance == null) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	/**
	 * Handles hooks.
	 */
	function init() {
		parent::init();

		add_filter('gform_confirmation', [$this, 'gform_confirmation'], 10, 4);
		add_filter('gform_data_layer', [$this, 'gform_data_layer'], 9, 2);
		add_filter('gform_form_tag', [$this, 'gform_tag'], 10, 2);
		add_filter('query_vars', [$this, 'query_vars']);
		add_action('wp_head', [$this, 'wp_head']);
	}

	function gform_confirmation($confirmation, $form, $entry, $is_ajax) {
		if (is_string($confirmation)) {
			$confirmation = $this->gform_text_confirmation($confirmation, $form, $entry);
		} else {
			$confirmation = $this->gform_url_confirmation($confirmation, $form, $entry);
		}

		return $confirmation;
	}

	private function data_layer_push_script($data_layer) {
		$tmpl = '<script>dataLayer=window.dataLayer||[];dataLayer.push(%s);</script>';
		return sprintf($tmpl, json_encode($data_layer));
	}

	private function gform_text_confirmation($confirmation, $form, $entry) {
			$data_layer = $this->get_form_data_layer($form, $entry);

			return $confirmation. $this->data_layer_push_script($data_layer);
	}

	private function gform_url_confirmation($confirmation, $form, $entry) {
		if (array_key_exists('redirect', $confirmation) && ($confirmation_url = $confirmation['redirect'])) {
			$salted_entry_id = $this->_salt . $entry['id'];
			$encrypted_entry_id = GFCommon::openssl_encrypt($salted_entry_id);
			$url_w_nonce = add_query_arg( '_wpnonce', wp_create_nonce($salted_entry_id), $confirmation_url);
			$conf_with_entry_id = sprintf('%s&%s=%s', $url_w_nonce, $this->_slug, urlencode($encrypted_entry_id));

			$confirmation['redirect'] = $conf_with_entry_id;
		}

		return $confirmation;
	}

	private function get_form_data_layer($form, $entry = false) {
		$data_layer = [
			'event' => 'gform-submit',
			'sales_cycle' => $form[$this->_slug]['sales_cycle']
		];

		if (array_key_exists($this->_slug, $form) && isset($form[$this->_slug]['data_layer_properties'])) {
			$data_layer = array_merge($data_layer, $form[$this->_slug]['data_layer_properties']);
		}

		if ($entry) {
			foreach ($data_layer as $dl_key => $dl_val) {
				if (GFCommon::has_merge_tag($dl_val)) {
					$data_layer[$dl_key] = GFCommon::replace_variables($dl_val, $form, $entry);
				}
			}
		}

		return apply_filters('gform_data_layer', $data_layer, $form);
	}

	function gform_data_layer($data_layer, $form) {
		$data_layer['form-title'] = $form['title'];

		return $data_layer;
	}

	function gform_tag($form_tag, $form) {
		$data_layer = $this->get_form_data_layer($form);

		foreach ($data_layer as $key => $val) {
			$atts[] = sprintf('data-gtm-%s="%s"', esc_attr($key), esc_attr($val));
		}

		if (count($atts) > 0) {
			$form_tag = str_replace('<form', '<form data-gtm ' . implode(' ', $atts), $form_tag);
		}

		return $form_tag;
	}

	function query_vars($query_vars) {
		$query_vars[] = $this->_slug;
		return $query_vars;
	}

	function wp_head() {
		if ($encrypted_entry_id = get_query_var($this->_slug)) {
			if (!$decrypted_entry_id = GFCommon::openssl_decrypt($encrypted_entry_id)) {
				return;
			}

			$entry_id = str_replace($this->_salt, '', $decrypted_entry_id);
			$entry = GFAPI::get_entry($entry_id);
			$already_pushed = gform_get_meta($entry_id, 'gfdl_pushed');

			if (!$already_pushed && ($nonce = $_GET['_wpnonce']) && wp_verify_nonce($nonce, $decrypted_entry_id)) {
				$form = GFAPI::get_form($entry['form_id']);
				$data_layer = $this->get_form_data_layer($form, $entry);

				echo $this->data_layer_push_script($data_layer);
				gform_update_meta($entry_id, 'gfdl_pushed', 1);
			}
		}
	}

	function scripts() {
		$scripts = [
			[
				'handle' => 'gfdl_admin_js',
				'src'    => $this->get_base_url() . '/assets/admin.js',
				'version' => $this->_version,
				'deps'    => ['jquery'],
				'in_footer' => true,
				'enqueue'   => [
					[
						'admin_page' => 'form_settings',
						'tab'        => $this->_slug,
					],
				],
			]
		];

		return array_merge(parent::scripts(), $scripts);
	}

	/**
	 * Configures the individual form settings
	 *
	 * @return array
	 */
	function form_settings_fields($form) {
		return [
			[
				'title'  => esc_html__('Google Tag Manager Data Layer', 'gravityforms-data-layer'),
				'fields' => [
					[
						'label'   => esc_html__('Sales Cycle', 'gravityforms-data-layer'),
						'type'    => 'select',
						'name'    => 'sales_cycle',
						'choices' => [
							['label' => esc_html__('None', 'gravityforms-data-layer'), 'value' => 'None'],
							['label' => esc_html__('Early Stage', 'gravityforms-data-layer'), 'value' => 'Early Stage'],
							['label' => esc_html__('Sales Ready', 'gravityforms-data-layer'), 'value' => 'Sales Ready'],
						],
						'tooltip' => esc_html__('Select the sales cycle stage for this form.', 'gravityforms-data-layer'),
					],
					[
						'label'   => esc_html__('Data Layer Properties', 'gravityforms-data-layer'),
						'type'    => 'data_layer_dictionary',
						'name'    => 'data_layer_properties',
						'default_value' => ['' => ''],
						'tooltip' => esc_html__('Enter a list of key/value pairs. These will be pushed to the GTM Data Layer whenever this form is submitted.', 'gravityforms-data-layer'),
					],
				],
			],
		];
	}

	function save_form_settings($form, $settings) {
		$output = [
			'sales_cycle' => rgar($settings, 'sales_cycle'),
			'data_layer_properties' => []
		];

		foreach ($_POST as $key => $val) {
			$key_matches = [];
			if (!preg_match("/(.*)_(\d+)_(key|val)$/i", $key, $key_matches)) {
				continue;
			}

			$index = $key_matches[2];
			$key_or_val = $key_matches[3];

			if (!array_key_exists($index, $output['data_layer_properties'])) {
				$output['data_layer_properties'][$index] = [];
			}

			$output['data_layer_properties'][$index][$key_or_val] = $val;
		}

		$settings = [
			'sales_cycle' => $output['sales_cycle'],
			'data_layer_properties' => array_reduce($output['data_layer_properties'], function($props, $row) {
				$props[$row['key']] = $row['val'];
				return $props;
			}, [])
		];

		return parent::save_form_settings($form, $settings);
	}

	function settings_data_layer_dictionary($field) {
		$props = $this->get_current_settings();
		$data_layer_properties = isset($props['data_layer_properties']) ? $props['data_layer_properties'] : $field['default_value'];

		$table_template = '<table data-name="%s"><tbody>%s<tr colspan="3"><td><button class="add-property-row gfbutton button-primary">Add Data Layer Property</button></td></tr></tbody></table>';
		$row_template = '<tr><td>%s</td><td>%s</td><td><button class="remove-property-row button-secondary">&times;</button></td></tr>';
		$input_template = '<input type="text" name="%s" value="%s">';

		$rows = [];
		$row_number = 0;
		foreach ($data_layer_properties as $key => $val) {
			$prefix = sprintf('%s_%d', $field['name'], $row_number);

			$key_input = sprintf($input_template, "{$prefix}_key", $key);
			$val_input = sprintf($input_template, "{$prefix}_val", $val);

			$rows[] = sprintf($row_template, $key_input, $val_input);
			$row_number++;
		}

		printf($table_template, $field['name'], implode('', $rows));
	}
}