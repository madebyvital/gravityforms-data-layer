(function($) {
	var NEW_ROW = '<tr><td><input type="text" name="data_layer_properties_0_key" value=""></td><td><input type="text" name="data_layer_properties_0_val" value=""></td><td><button class="remove-property-row button-secondary">&times;</button></td></tr>';
	var SELECTOR = "#gform_setting_data_layer_properties";
	var el = null;

	function onDocumentReady() {
		el = $(SELECTOR);
		if (el.length > 0) {
			init();
		}
	}

	function init() {
		el
		 .on('click', '.remove-property-row', onClickRemoveRow)
		 .on('click', '.add-property-row', onClickAddRow);
	}

	function onClickRemoveRow(e) {
		$(e.currentTarget).closest('tr').remove();
		renumberDataRows();

		e.preventDefault();
		return false;
	}

	function onClickAddRow(e) {
		$(e.currentTarget).closest('tr').before(NEW_ROW);
		renumberDataRows();

		e.preventDefault();
		return false;
	}

	function renumberDataRows() {
		el.find('tr').each(function(i, tr) {
			$(tr).find('input[type="text"]').each(function(j, input) {
				var inputName = input.getAttribute('name');
				var newName = inputName.replace(/^data_layer_properties_\d+/, 'data_layer_properties_' + i);

				input.setAttribute("name", newName);
			});
		});
	}

	document.addEventListener("DOMContentLoaded", onDocumentReady);
})(window.jQuery);


(function($) {
    function onDocumentReady() {
        if ($('#gfdl-bulk-editor').length > 0) {
            initBulkEditor();
        }
    }

    function initBulkEditor() {
        $('form').on('submit', function() {
            // Add any validation or processing logic here
        });
    }

    document.addEventListener("DOMContentLoaded", onDocumentReady);
})(window.jQuery);
