<?php
/*
 * Plugin Name: Gravity Forms Data Layer
 * Version: 1.2
 * Plugin URI: https://vtldesign.com/
 * Description: Add a tab to Form Settings that allows you to add custom key/value pairs to be pushed into the GTM Data Layer upon form submission.
 * Author: Vital
 * Author URI: https://vtldesign.com/
 * Requires at least: 5.0
 * Tested up to: 6.2
 * Text Domain: gravityforms-data-layer
 * Domain Path: /lang/
 */

if (!defined("ABSPATH")) {
    exit();
}

define("GFDL_VERSION", "1.2");
define("GFDL_PLUGIN_DIR", plugin_dir_path(__FILE__));
define("GFDL_PLUGIN_URL", plugin_dir_url(__FILE__));

add_action("gform_loaded", ["Gravity_Forms_Data_Layer", "load"], 5);

class Gravity_Forms_Data_Layer
{
    public static function load()
    {
        if (!method_exists("GFForms", "include_addon_framework")) {
            return;
        }

        require_once "class-gravityforms-data-layer.php";

        GFAddOn::register("GFDL");
    }
}

// Add menu item to Gravity Forms menu
add_filter("gform_addon_navigation", function ($menu_items) {
    $menu_items[] = [
        "name" => "gfdl_bulk_editor",
        "label" => "Bulk Edit Data Layers",
        "callback" => "gfdl_render_bulk_editor_page",
        "permission" => "manage_options",
    ];

    return $menu_items;
});

// Enqueue admin assets
add_action("admin_enqueue_scripts", function ($hook) {
    // Only load on our bulk editor page
    if ($hook === "forms_page_gfdl_bulk_editor") {
        wp_enqueue_style(
            "gfdl-bulk-editor-css",
            GFDL_PLUGIN_URL . "assets/bulk-editor.css",
            [],
            GFDL_VERSION
        );
    }
});

function gfdl_render_bulk_editor_page()
{
    if (!current_user_can("manage_options")) {
        wp_die(
            __("You do not have sufficient permissions to access this page.")
        );
    }

    require_once plugin_dir_path(__FILE__) . "class-gfdl-bulk-editor.php";
    GFDL_Bulk_Editor::render_bulk_editor();
}
